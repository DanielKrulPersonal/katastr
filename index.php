<?php
require_once 'KatastralniUzemi.php';
$downloadList = file_get_contents('downloadList.txt');
$downloadListParsed = explode(PHP_EOL, $downloadList);

if(is_file('vysledek.csv') && empty(KatastralniUzemi::ziskejSpusteneProcesy())) {
	$hash = md5_file('vysledek.csv');
	if(!in_array($hash, $downloadListParsed)) {
		header('Location: stahnoutVysledek.php');
	} else {
		@unlink('vysledek.csv');
	}
}
?>

<body>
	<form action="zpracovaniKatastralnichUzemi.php" method="POST" enctype="multipart/form-data">
		<label>
			Vstupní soubor:
			<input name="csvFile" type="file" accept=".csv, .CSV" required="required">
		</label>

		<button type="submit" name="upload" value="1">Nahrát</button>
	</form>
</body>
