<?php

class VyberParcely {
    const URL = 'https://nahlizenidokn.cuzk.cz/VyberParcelu.aspx';
    private $cookie = '';
    private $firstFields = [];
    private $secondFields = [];
    public $url = '';

    public function __construct($place, $arcis, $druhCislovani = null)
    {
        $this->getCookie();
        $place = $this->getName($place);

        $this->firstStep($place);
        $this->secondStep($arcis, $druhCislovani);
        $this->url = $this->thirdStep();
    }

    public function getName($place) {
        $name = json_decode(file_get_contents('https://nahlizenidokn.cuzk.cz/AutoCompleteKUHandler.ashx?term=' . urlencode($place)));
        if(count($name) > 0) return $name[0];
        return $place;
    }

    public function getCookie() {
        $ch = curl_init(self::URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($ch);
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);        // get cookie
        $cookies = array();
        foreach($matches[1] as $item) {
            parse_str($item, $cookie);
            $cookies = array_merge($cookies, $cookie);
        }
        curl_close($ch);
        $this->cookie = $cookies['ASP_NET_SessionId'];
    }

    public function firstStep($place) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Cookie: ASP.NET_SessionId=' . $this->cookie,
            )
        );
        $response = curl_exec($ch);
        $data = str_get_html('<meta charset="UTF-8">' . $response);
        curl_close($ch);

        $viewstate = $data->find('#__VIEWSTATE', 0);
        $viewstateGenerator = $data->find('#__VIEWSTATEGENERATOR', 0);
        $eventvalidation = $data->find('#__EVENTVALIDATION', 0);

        $this->firstFields = array(
            'ctl00$bodyPlaceHolder$vyberObecKU$vyberKU$txtKU' => $place,
            'ctl00$bodyPlaceHolder$vyberObecKU$vyberKU$btnKU' => 'Vyhledat',
            '__VIEWSTATEGENERATOR' => $viewstateGenerator->value,
            '__EVENTVALIDATION' => $eventvalidation->value,
            '__VIEWSTATE' => $viewstate->value
        );
    }

    public function secondStep($arcis, $druhCislovani) {
        $arcisParts = explode('/', $arcis);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Cookie: ASP.NET_SessionId=' . $this->cookie,
            )
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->firstFields));

        $response = curl_exec($ch);
        $data = str_get_html('<meta charset="UTF-8">' . $response);
        curl_close($ch);
        $viewstate = $data->find('#__VIEWSTATE', 0);
        $viewstateGenerator = $data->find('#__VIEWSTATEGENERATOR', 0);
        $eventvalidation = $data->find('#__EVENTVALIDATION', 0);

        /*$druhCislovaniItem = $data->find('input[name="ctl00$bodyPlaceHolder$druhCislovani"][checked="checked"]', 0);

        if(!isset($druhCislovaniItem->value) && !$druhCislovani) {
            $druhCislovani = null;
        }*/
        if($druhCislovani == '2') $druhCislovani = null;

        if($data->find('input[name="ctl00$bodyPlaceHolder$typParcely"][checked="checked"]', 0)) {
            $this->secondFields = array(
                '__VIEWSTATEGENERATOR' => $viewstateGenerator->value,
                '__EVENTVALIDATION' => $eventvalidation->value,
                '__VIEWSTATE' => $viewstate->value,
                'ctl00$bodyPlaceHolder$typParcely' => $data->find('input[name="ctl00$bodyPlaceHolder$typParcely"][checked="checked"]', 0)->value,
                'ctl00$bodyPlaceHolder$druhCislovani' => $druhCislovani,
                'ctl00$bodyPlaceHolder$txtParpod' => array_key_exists(1, $arcisParts) ? $arcisParts[1] : '',
                'ctl00$bodyPlaceHolder$txtParcis' => $arcisParts[0],
                'ctl00$bodyPlaceHolder$btnVyhledat' => 'Vyhledat'
            );
        } else {
            $this->secondFields = array(
                '__VIEWSTATEGENERATOR' => $viewstateGenerator->value,
                '__EVENTVALIDATION' => $eventvalidation->value,
                '__VIEWSTATE' => $viewstate->value,
                'ctl00$bodyPlaceHolder$druhCislovani' => $druhCislovani,
                'ctl00$bodyPlaceHolder$txtParpod' => array_key_exists(1, $arcisParts) ? $arcisParts[1] : '',
                'ctl00$bodyPlaceHolder$txtParcis' => $arcisParts[0],
                'ctl00$bodyPlaceHolder$btnVyhledat' => 'Vyhledat'
            );
        }


        //echo $data->save();
    }

    public function thirdStep() {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Cookie: ASP.NET_SessionId=' . $this->cookie,
            )
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->secondFields));

        curl_exec($ch);
        $response = curl_getinfo($ch);
        curl_close($ch);
        return $response['redirect_url'];
    }

    public function getVlastnik () {
        if(!$this->url) return '';

        $data = file_get_contents($this->url);
        $data = str_get_html('<meta charset="UTF-8">' . $data);

        $vlastnik = $data->find('table[summary="Vlastníci, jiní oprávnění"] tr');
        $vlastnikCely = '';
        foreach($vlastnik as $tr) {
            $vlastnik = $tr->find('td', 0);
            if($vlastnik) {
                $vlastnikCely .= $vlastnik->text;

                $podil = $tr->find('td', 1);
                if($podil->text) {
                    $vlastnikCely .= ' (' . $podil->text . ')';
                }
                $vlastnikCely .= "\n";
            }
        }

        return rtrim($vlastnikCely);
    }
}
