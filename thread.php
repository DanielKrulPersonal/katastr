<?php
$opts  = '';
$opts .= 'u:';
$opts .= 's:';
$opts .= 'd:';

require_once 'DetailParcely.php';

$options = getopt($opts);
if($options) {
    $uzemi = $options['u'];
    $soubor = $options['s'];
    $identifikaceList = json_decode($options['d']);

    cli_set_process_title("katastr thread {$uzemi} {$soubor}");

    $identifikaceInstance = new DetailParcely($soubor, $identifikaceList);
    $identifikaceInstance->run();
}
