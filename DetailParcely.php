<?php
require_once 'config.php';
require_once 'VyberParcely.php';

class DetailParcely {
    const ID_PARCELY = 0;
    const DRUH_CISLOVANI = 1;
    const KATASTRALNI_UZEMI = 5;
    const NAZEV_OBCE = 7;
    const URL_DETAIL = 'https://vdp.cuzk.cz/vdp/ruian/parcely/';

    private $lines = [];
    private $pozadovaneIdentifikaceList = [];

    public function __construct($zdrojovySoubor, $identifikaceList)
    {
        $zdrojovySoubor = file_get_contents($zdrojovySoubor);
        $this->lines = explode(PHP_EOL, $zdrojovySoubor);
        $this->pozadovaneIdentifikaceList = $identifikaceList;
        array_shift($this->lines); // preskocit hlavicku
    }

    public function run() {
        foreach ($this->lines as $line) {
            if (empty($line))
                continue;

            $data = str_getcsv($line, ";");
            $urlDetail = self::URL_DETAIL . $data[self::ID_PARCELY];
            $vysledek = $this->zpracujUrl($urlDetail);
            $druhCislovani = $data[self::DRUH_CISLOVANI];

	        $identifikace = $this->ziskejIdentifikaci($vysledek['katastralniUzemiKod'], $vysledek['obecKod'], $data[self::ID_PARCELY]);
	        if ($identifikace && strpos($identifikace, 'č.p.') === false && strpos($identifikace, 'č.ev.') === false && in_array($identifikace, $this->pozadovaneIdentifikaceList)) {
                $this->zapisDoCSV($identifikace, $data, $druhCislovani);
            }
        }
    }

    private function zapisDoCSV($identifikace, $data, $druhCislovani) {
        if(!is_file('vysledek.csv')){
            file_put_contents('vysledek.csv', "Obec;Katastrální území;Parcela;Identifikace;Vlastník\n");
        }

        $nazevObce = $data[self::NAZEV_OBCE];
        $katastralniUzemi = $data[self::KATASTRALNI_UZEMI];
        $parcela = empty($data[3]) ? $data[2] : $data[2] . '/' . $data[3];

        $dataKZapsani = [$nazevObce, $katastralniUzemi, $parcela, $identifikace];
        $vysledekObsah = file_get_contents('vysledek.csv');
        $vysledekLine = explode(PHP_EOL, $vysledekObsah);
        $obsahuje = false;

        foreach ($vysledekLine as $line) {
            if (empty($line))
                continue;

            $vysledek = str_getcsv($line, ";");
            if($vysledek[0] == $dataKZapsani[0] && $vysledek[1] == $dataKZapsani[1] && $vysledek[2] == $dataKZapsani[2] && $vysledek[3] == $dataKZapsani[3]) $obsahuje = true;
        }

        if(!$obsahuje) {
            $fp = fopen('vysledek.csv', 'a+');
            $vlastnikInstance = new VyberParcely($katastralniUzemi, $parcela, $druhCislovani);
            $vlastnik = $vlastnikInstance->getVlastnik();
            $dataKZapsani[4] = $vlastnik;

            file_put_contents('zpracovanaKlicovaSlova.txt', file_get_contents('zpracovanaKlicovaSlova.txt') . "{$katastralniUzemi} - {$identifikace}\n");

            fputcsv($fp, $dataKZapsani, ';');
            fclose($fp);
        }
    }

    private function zpracujUrl($url) {
        $web = file_get_html($url);
        $katastralniUzemiRaw = $web->find('a[title="Zobrazí detail katastrálního území."]', 0);
        $obecRaw = $web->find('a[title="Zobrazí detail obce."]', 0);
        $obecKod = filter_var($obecRaw->href, FILTER_SANITIZE_NUMBER_INT);
        $katastralniUzemiKod = filter_var($katastralniUzemiRaw->href, FILTER_SANITIZE_NUMBER_INT);

        if(!$katastralniUzemiRaw || !$obecRaw) {
        	var_dump($url);
        	exit;
        }

        $data = [
            'obec' => $obecRaw->innertext,
            'obecKod' => $obecKod,
            'katastralniUzemiKod' => $katastralniUzemiKod,
            'katastralniUzemi' => $katastralniUzemiRaw->innertext,
            'parcela' => str_replace('st.', '', $web->find('.detail td', 5)->innertext)
        ];
        return $data;
    }

    private function ziskejIdentifikaci($katastralniUzemiKod, $obecKod, $idParcely) {
        $url = "https://vdp.cuzk.cz/vdp/ruian/stavebniobjekty/vyhledej?ob.kod={$obecKod}&co.kod=&mc.kod=&ku.kod={$katastralniUzemiKod}&pa.parcelaId={$idParcely}&search=Vyhledat";
        $web = file_get_html($url);
        $identifikace = $web->find('.o td', 1);
        if($identifikace) return $identifikace->plaintext;
        return '';
    }
}
