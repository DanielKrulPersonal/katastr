<?php

function request($key, $value = null) {
    if (isset($_REQUEST[$key]))
        return $_REQUEST[$key];
    else if (isset($_REQUEST[$key = strtr($key, '.', '_')]))
        return $_REQUEST[$key];
    else
        return $value;
}

function requestParameter($key, $value = null) {
    return request($key, $value);
}