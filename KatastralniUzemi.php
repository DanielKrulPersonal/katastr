<?php
class KatastralniUzemi {
    const CSV_KATASTRALNI_UZEMI = 0;
    const CSV_IDENTIFIKACE = 1;
    const CACHE_LOCATION = 'cache/';
    const CACHE_INTERVAL = 1296000; // 15 dní

    private $vstupniSoubor;
    private $uzemiIdentifikace = [];
    private $souboryKeZpracovani = [];

    public function __construct(string $soubor)
    {
        $this->vstupniSoubor = file_get_contents($soubor);
        $this->zpracujSoubor();
    }

    private function ziskejKodUzemi(string $nazevUzemi): string {
        $plneJmeno = json_decode(file_get_contents('https://nahlizenidokn.cuzk.cz/AutoCompleteKUHandler.ashx?term=' . urlencode($nazevUzemi)))[0];
        $plneJmenoCasti = explode(';', $plneJmeno);
        return $plneJmenoCasti[1];
    }

    private function zpracujSoubor() {
        $radky = explode(PHP_EOL, $this->vstupniSoubor);

        foreach ($radky as $radek) {
            if (empty($radek))
                continue;

            $data = str_getcsv($radek, ";");
            $this->zpracujRadek($data);
        }

        $this->zpracujUzemiIdentifikace();
    }

    private function zpracujRadek(array $data) {
        $katastralniUzemi = $data[self::CSV_KATASTRALNI_UZEMI];

        foreach ($data as $index => $hodnota) {
            if($index === self::CSV_KATASTRALNI_UZEMI || !$hodnota) continue;

            $this->uzemiIdentifikace[$katastralniUzemi][] = $hodnota;
        }
    }

    private function zpracujUzemiIdentifikace() {
        ob_end_flush();
        ob_implicit_flush();

        StringTemplates::t('PROBEHNUVSI_AKCE');

        foreach ($this->uzemiIdentifikace as $nazevUzemi => $uzemiIdentifikace) {
            $kodUzemi = $this->ziskejKodUzemi($nazevUzemi);

            $cacheJmenoSouboru = self::CACHE_LOCATION . $kodUzemi . '.csv';
            $this->souboryKeZpracovani[$nazevUzemi] = [
                'soubor' => $cacheJmenoSouboru,
                'identifikace' => json_encode($uzemiIdentifikace)
            ];

            if(!is_file($cacheJmenoSouboru)) {
                $this->ziskejAUlozParcely($kodUzemi);
            } else {
                $casZmeny = filemtime($cacheJmenoSouboru);
                if(time() > ($casZmeny + self::CACHE_INTERVAL)) {
                    $this->ziskejAUlozParcely($kodUzemi);
                }
            }

            StringTemplates::t('STAZENO', $nazevUzemi);
        }

        $this->zpracujStazeneParcely();
    }

    private function jeProces($jmenoProcesu) {
        exec("pidof '{$jmenoProcesu}'", $out);
        if($out) return true;
        return false;
    }

    static function ziskejSpusteneProcesy() {
        if(!is_file('spusteneProcesy.json')) file_put_contents('spusteneProcesy.json', '');

        $spusteneProcesySoubor = file_get_contents('spusteneProcesy.json');
        $spusteneProcesy = json_decode($spusteneProcesySoubor);
        if(!is_array($spusteneProcesy)) $spusteneProcesy = [];
        return $spusteneProcesy;
    }

    private function zpracujStazeneParcely() {
        $spusteneProcesy = self::ziskejSpusteneProcesy();

        foreach ($this->souboryKeZpracovani as $nazevUzemi => $data) {
            $jmenoProcesu = "katastr thread {$nazevUzemi} {$data['soubor']}";
            if(!$this->jeProces($jmenoProcesu) && !in_array($jmenoProcesu, $spusteneProcesy)) {
                exec("php thread.php -u '{$nazevUzemi}' -s '{$data['soubor']}' -d '{$data['identifikace']}' > /tmp/katastrLog.txt &");
                $spusteneProcesy[] = $jmenoProcesu;
            }
        }
        file_put_contents('spusteneProcesy.json', json_encode($spusteneProcesy));

        $this->vypisZpracovanaKlicovaSlova();
    }

    private function vypisZpracovanaKlicovaSlova() {
        StringTemplates::break(3);
        StringTemplates::t('KLICOVA_SLOVA');
        $klicovaSlovaList = [];
        $vsechnyProcesyUkonceny = false;
        sleep(5); // interval než jsou všechny procesy spuštěny

        while(true) {
            $klicovaSlova = @file_get_contents('zpracovanaKlicovaSlova.txt');
            $klicovaSlovaParts = explode(PHP_EOL, $klicovaSlova);
            foreach ($klicovaSlovaParts as $klicoveSlovo) {
                if(!in_array($klicoveSlovo, $klicovaSlovaList) && $klicoveSlovo) {
                    $klicovaSlovaList[] = $klicoveSlovo;
                    echo $klicoveSlovo . ' | ';
                }

                foreach ($this->souboryKeZpracovani as $nazevUzemi => $data) {
                    if(!$this->jeProces("katastr thread {$nazevUzemi} {$data['soubor']}")) {
                        $vsechnyProcesyUkonceny = true;
                    } else {
                        $vsechnyProcesyUkonceny = false;
                    }
                }
            }

            if($vsechnyProcesyUkonceny) {
                file_put_contents('spusteneProcesy.json', '');
                file_put_contents('zpracovanaKlicovaSlova.txt', '');
                header('Location: stahnoutVysledek.php');
                break;
            };
        }
    }

    private function ziskejAUlozParcely(string $kodUzemi) {
        $url = 'https://vdp.cuzk.cz/vdp/ruian/parcely/export?ob.nazev=&ku.kod=%s&pa.druhCislovaniPa=&pa.kmCis=&pa.podCis=&ohrada.id=&pag.sort=PARCELA1&export=CSV';
        $url = str_replace('%s', $kodUzemi, $url);
        $parcelyCSV = file_get_contents($url);
        file_put_contents(self::CACHE_LOCATION . $kodUzemi . '.csv', $parcelyCSV);
    }
}