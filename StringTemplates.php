<?php
class StringTemplates {
    static $texty = [
        'PROBEHNUVSI_AKCE' => '<h2>Zatím proběhly tyto akce: </h2>',
        'STAZENO' => '<li>staženo exportní CSV pro katastrální území: <strong>%s</strong> </li>',
        'KLICOVA_SLOVA' => 'Zatím byla zpracována tato klíčová slova: '
    ];

    static function t($index, $hodnota = null) {
        echo str_replace('%s', $hodnota, self::$texty[$index]);
    }

    static function break($count) {
        echo str_repeat('<br>', $count);
    }
}