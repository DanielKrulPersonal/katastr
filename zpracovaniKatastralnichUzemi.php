<?php
require_once 'config.php';
require_once 'utils.php';
require_once 'KatastralniUzemi.php';
require_once 'StringTemplates.php';

if($_FILES['csvFile']) {
    $soubor = $_FILES['csvFile']['tmp_name'];
    $katastralniUzemiInstance = new KatastralniUzemi($soubor);
} else {
    header('Location: index.php');
}